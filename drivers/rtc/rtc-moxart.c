/* Copyright (C) 2013 Jonas Jensen <jonas.jensen@gmail.com>
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version. */

#include <linux/bcd.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/jiffies.h>
#include <linux/rtc.h>
#include <linux/platform_device.h>
#include <linux/io.h>

#include <mach/hardware.h>
#include <mach/gpio.h>

#define DRV_VERSION "1.0"

#define RTC_REG_SIZE		0x1000

struct rtc_plat_data {
	struct rtc_device *rtc;
	void __iomem *ioaddr;
	resource_size_t baseaddr;
	unsigned long last_jiffies;
	int irq;
	unsigned int irqen;
	int alrm_sec;
	int alrm_min;
	int alrm_hour;
	int alrm_mday;
};

static spinlock_t rtc_lock;
static int day_of_year[12] = {0, 31, 59, 90, 120, 151, 181,
		212, 243, 273, 304, 334};

static u8 moxart_rtc_read_register(u8 cmd)
{
	u8 data;
	unsigned long flags;

	local_irq_save(flags);

	moxart_gpio_inout(GPIO_RTC_DATA, GPIO_EM1240_OUTPUT);
	moxart_gpio_set(GPIO_RTC_RESET, GPIO_EM1240_HIGH);
	udelay(GPIO_RTC_DELAY_TIME);

	moxart_gpio_write_byte(cmd);

	moxart_gpio_inout(GPIO_RTC_DATA, GPIO_EM1240_INPUT);
	udelay(GPIO_RTC_DELAY_TIME);

	data = moxart_gpio_read_byte();

	moxart_gpio_set(GPIO_RTC_SCLK, GPIO_EM1240_LOW);
	moxart_gpio_set(GPIO_RTC_RESET, GPIO_EM1240_LOW);
	udelay(GPIO_RTC_DELAY_TIME);

	local_irq_restore(flags);

	return data;
}

static void moxart_rtc_write_register(u8 cmd, u8 data)
{
	unsigned long flags;

	local_irq_save(flags);

	moxart_gpio_inout(GPIO_RTC_DATA, GPIO_EM1240_OUTPUT);
	moxart_gpio_set(GPIO_RTC_RESET, GPIO_EM1240_HIGH);
	udelay(GPIO_RTC_DELAY_TIME);

	moxart_gpio_write_byte(cmd);

	moxart_gpio_inout(GPIO_RTC_DATA, GPIO_EM1240_OUTPUT);

	moxart_gpio_write_byte(data);

	moxart_gpio_set(GPIO_RTC_SCLK, GPIO_EM1240_LOW);
	moxart_gpio_set(GPIO_RTC_RESET, GPIO_EM1240_LOW);
	udelay(GPIO_RTC_DELAY_TIME);
	/* moxart_gpio_inout(GPIO_RTC_DATA, GPIO_EM1240_INPUT); */

	local_irq_restore(flags);
}

static int moxart_rtc_set_time(struct device *dev, struct rtc_time *tm)
{
	spin_lock_irq(&rtc_lock);

	moxart_rtc_write_register(GPIO_RTC_PROTECT_W, 0);
	moxart_rtc_write_register(GPIO_RTC_YEAR_W,
		((((tm->tm_year - 100) / 10) << 4) |
		  ((tm->tm_year - 100) % 10)));
	moxart_rtc_write_register(GPIO_RTC_MONTH_W,
		((((tm->tm_mon + 1) / 10) << 4) | ((tm->tm_mon + 1) % 10)));
	moxart_rtc_write_register(GPIO_RTC_DATE_W,
		(((tm->tm_mday / 10) << 4) | (tm->tm_mday % 10)));
	moxart_rtc_write_register(GPIO_RTC_HOURS_W,
		(((tm->tm_hour / 10) << 4) | (tm->tm_hour % 10)));
	moxart_rtc_write_register(GPIO_RTC_MINUTES_W,
		(((tm->tm_min / 10) << 4) | (tm->tm_min % 10)));
	moxart_rtc_write_register(GPIO_RTC_SECONDS_W,
		(((tm->tm_sec / 10) << 4) | (tm->tm_sec % 10)));
	moxart_rtc_write_register(GPIO_RTC_PROTECT_W, 0x80);

	spin_unlock_irq(&rtc_lock);

	dbg_printk(KERN_INFO
		"MOXART RTC: moxart_rtc_set_time success tm_year=%d tm_mon=%d\n"
		"tm_mday=%d tm_hour=%d tm_min=%d tm_sec=%d\n",
		tm->tm_year, tm->tm_mon, tm->tm_mday, tm->tm_hour,
		tm->tm_min, tm->tm_sec);

	return 0;
}

static int moxart_rtc_read_time(struct device *dev, struct rtc_time *tm)
{
	unsigned char v;

	spin_lock_irq(&rtc_lock);

	v = moxart_rtc_read_register(GPIO_RTC_SECONDS_R);
	tm->tm_sec = (((v & 0x70) >> 4) * 10) + (v & 0x0F);
	v = moxart_rtc_read_register(GPIO_RTC_MINUTES_R);
	tm->tm_min = (((v & 0x70) >> 4) * 10) + (v & 0x0F);
	v = moxart_rtc_read_register(GPIO_RTC_HOURS_R);
	if (v & 0x80) { /* 12-hour mode */
		tm->tm_hour = (((v & 0x10) >> 4) * 10) + (v & 0x0F);
		if (v & 0x20) { /* PM mode */
			tm->tm_hour += 12;
			if (tm->tm_hour >= 24)
				tm->tm_hour = 0;
		}
	} else { /* 24-hour mode */
		tm->tm_hour = (((v & 0x30) >> 4) * 10) + (v & 0x0F);
	}
	v = moxart_rtc_read_register(GPIO_RTC_DATE_R);
	tm->tm_mday = (((v & 0x30) >> 4) * 10) + (v & 0x0F);
	v = moxart_rtc_read_register(GPIO_RTC_MONTH_R);
	tm->tm_mon = (((v & 0x10) >> 4) * 10) + (v & 0x0F);
	tm->tm_mon--;
	v = moxart_rtc_read_register(GPIO_RTC_YEAR_R);
	tm->tm_year = (((v & 0xF0) >> 4) * 10) + (v & 0x0F);
	tm->tm_year += 100;
	if (tm->tm_year <= 69)
		tm->tm_year += 100;
	v = moxart_rtc_read_register(GPIO_RTC_DAY_R);
	tm->tm_wday = (v & 0x0f) - 1;
	tm->tm_yday = day_of_year[tm->tm_mon];
	tm->tm_yday += (tm->tm_mday - 1);
	if (tm->tm_mon >= 2) {
		if (!(tm->tm_year % 4) && (tm->tm_year % 100))
			tm->tm_yday++;
	}
	tm->tm_isdst = 0;

	spin_unlock_irq(&rtc_lock);

	return 0;
}

static int moxart_rtc_ioctl(struct device *dev, unsigned int cmd,
	unsigned long arg)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct rtc_plat_data *pdata = platform_get_drvdata(pdev);

	if (pdata->irq <= 0)
		return -ENOIOCTLCMD; /* fall back into rtc-dev's emulation */
	switch (cmd) {
	default:
		return -ENOIOCTLCMD;
	}
	return 0;
}

static const struct rtc_class_ops moxart_rtc_ops = {
	.read_time	= moxart_rtc_read_time,
	.set_time	= moxart_rtc_set_time,
	.ioctl		= moxart_rtc_ioctl,
};

static int moxart_rtc_probe(struct platform_device *pdev)
{
	struct rtc_device *rtc;
	struct resource *res;
	struct rtc_plat_data *pdata = NULL;
	void __iomem *ioaddr = NULL;
	int ret = 0;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res) {
		dev_info(&pdev->dev,
			"MOXART RTC: platform_get_resource failed\n");
		return -ENODEV;
	}
	pdata = devm_kzalloc(&pdev->dev, sizeof(*pdata), GFP_KERNEL);
	if (!pdata) {
		dev_info(&pdev->dev, "MOXART RTC: devm_kzalloc failed\n");
		return -ENOMEM;
	}
	if (!request_mem_region(res->start, RTC_REG_SIZE, pdev->name)) {
		ret = -EBUSY;
		dev_info(&pdev->dev, "MOXART RTC: request_mem_region failed\n");
		goto out;
	}
	pdata->baseaddr = res->start;
	ioaddr = ioremap(pdata->baseaddr, RTC_REG_SIZE);
	if (!ioaddr) {
		ret = -ENOMEM;
		dev_info(&pdev->dev, "MOXART RTC: ioremap failed\n");
		goto out;
	}
	pdata->ioaddr = ioaddr;

	moxart_gpio_mp_set(GPIO_RTC_MASK);
	moxart_gpio_inout(GPIO_RTC_RESET, GPIO_EM1240_OUTPUT);
	moxart_gpio_inout(GPIO_RTC_SCLK, GPIO_EM1240_OUTPUT);

	rtc = rtc_device_register(pdev->name, &pdev->dev,
		&moxart_rtc_ops, THIS_MODULE);
	if (IS_ERR(rtc)) {
		ret = PTR_ERR(rtc);
		goto out;
	}
	pdata->rtc = rtc;
	pdata->last_jiffies = jiffies;
	platform_set_drvdata(pdev, pdata);

	spin_lock_init(&rtc_lock);

	dev_info(&pdev->dev, "MOXART RTC: finished moxart_rtc_probe\n");

	if (ret)
		goto out;

	return 0;

 out:
	if (pdata->rtc)
		rtc_device_unregister(pdata->rtc);
	if (ioaddr)
		iounmap(ioaddr);
	if (pdata->baseaddr)
		release_mem_region(pdata->baseaddr, RTC_REG_SIZE);
	devm_kfree(&pdev->dev, pdata);
	return ret;
}

static int moxart_rtc_remove(struct platform_device *pdev)
{
	struct rtc_plat_data *pdata = platform_get_drvdata(pdev);

	rtc_device_unregister(pdata->rtc);
	iounmap(pdata->ioaddr);
	release_mem_region(pdata->baseaddr, RTC_REG_SIZE);
	devm_kfree(&pdev->dev, pdata);
	return 0;
}

/* work with hotplug and coldplug */
MODULE_ALIAS("platform:rtc-moxart");

static struct platform_driver moxart_rtc_driver = {
	.probe		= moxart_rtc_probe,
	.remove		= __devexit_p(moxart_rtc_remove),
	.driver		= {
		.name	= "MOXART_RTC",
		.owner	= THIS_MODULE,
	},
};

static __init int moxart_init(void)
{
	return platform_driver_register(&moxart_rtc_driver);
}

static __exit void moxart_exit(void)
{
	platform_driver_unregister(&moxart_rtc_driver);
}

module_init(moxart_init);
module_exit(moxart_exit);

MODULE_AUTHOR("Jonas Jensen <jonas.jensen@gmail.com>");
MODULE_DESCRIPTION("MOXART RTC driver");
MODULE_LICENSE("GPL");
MODULE_VERSION(DRV_VERSION);
