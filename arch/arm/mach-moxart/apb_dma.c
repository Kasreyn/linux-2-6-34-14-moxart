/* Copyright (C) 2013 Jonas Jensen <jonas.jensen@gmail.com>
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version. */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/io.h>
#include <linux/irq.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>

#include <asm/cacheflush.h>

#include <mach/hardware.h>
#include <mach/irqs.h>
#include <mach/apb_dma.h>

static struct apb_dma_priv	apb_dma_channel[APB_DMA_MAX_CHANNEL];
static spinlock_t apb_dma_lock;

struct apb_dma_priv *apb_dma_alloc(int req_no)
{
	int i;
	unsigned long flags;
	struct apb_dma_priv *priv = apb_dma_channel;

	spin_lock_irqsave(&apb_dma_lock, flags);
	for (i = 0; i < APB_DMA_MAX_CHANNEL; i++, priv++) {
		if (priv->used_flag == 0) {
			priv->used_flag = 1;
			priv->irq_handler = NULL;
			priv->irq_handler_param = NULL;
			priv->error_flag = 0;
			priv->req_no = req_no;
			switch (req_no) {
			case APB_DMA_SD_REQ_NO:
				*(unsigned int *) (IO_ADDRESS(MOXART_PMU_BASE)
					+ 0xB8) = 0;
				break;
			}
			spin_unlock_irqrestore(&apb_dma_lock, flags);
			dbg_printk(KERN_INFO
			"MOXART APB DMA: apb_dma_alloc uses DMA channel %d\n"
			, i);
			return priv;
		}
	}
	spin_unlock_irqrestore(&apb_dma_lock, flags);
	return NULL;
}

void apb_dma_release(struct apb_dma_priv *priv)
{
	unsigned long flags;

	spin_lock_irqsave(&apb_dma_lock, flags);
	if (priv == NULL) {
		spin_unlock_irqrestore(&apb_dma_lock, flags);
		return;
	}
	priv->used_flag = 0;
	priv->error_flag = 0;
	priv->irq_handler = NULL;
	priv->irq_handler_param = NULL;
	priv->req_no = 0;
	spin_unlock_irqrestore(&apb_dma_lock, flags);
}

void apb_dma_set_irq(struct apb_dma_priv *priv,
	void (*func)(void *param), void *param)
{
	unsigned long flags;
	unsigned int cmd;

	spin_lock_irqsave(&apb_dma_lock, flags);
	if (priv == NULL) {
		spin_unlock_irqrestore(&apb_dma_lock, flags);
		return;
	}
	priv->irq_handler = func;
	priv->irq_handler_param = param;
	priv->error_flag = 0;
	cmd = readl(&priv->reg->command.ul);
	cmd |= (APB_DMA_FIN_INT_EN | APB_DMA_ERR_INT_EN);
	writel(cmd, &priv->reg->command.ul);
	spin_unlock_irqrestore(&apb_dma_lock, flags);
}

void apb_dma_release_irq(struct apb_dma_priv *priv)
{
	unsigned long flags;
	unsigned int cmd;

	spin_lock_irqsave(&apb_dma_lock, flags);
	if (priv == NULL) {
		spin_unlock_irqrestore(&apb_dma_lock, flags);
		return;
	}
	cmd = readl(&priv->reg->command.ul);
	cmd &= ~(APB_DMA_FIN_INT_EN | APB_DMA_ERR_INT_EN);
	writel(cmd, &priv->reg->command.ul);
	priv->irq_handler = NULL;
	priv->irq_handler_param = NULL;
	priv->error_flag = 0;
	spin_unlock_irqrestore(&apb_dma_lock, flags);
}

void apb_dma_conf(struct apb_dma_priv *priv, struct apb_dma_conf_param *param)
{
	unsigned long flags;
	union _command cmd;
	unsigned int size;

	spin_lock_irqsave(&apb_dma_lock, flags);

	if (param->source_sel == APB_DMAB_SOURCE_AHB) {
		writel(virt_to_phys((void *)param->source_addr),
			&priv->reg->source_addr);
	} else
		writel(PHY_ADDRESS(param->source_addr),
			&priv->reg->source_addr);

	if (param->dest_sel == APB_DMAB_DEST_AHB) {
		writel(virt_to_phys((void *)param->dest_addr),
			&priv->reg->dest_addr);
	} else
		writel(PHY_ADDRESS(param->dest_addr), &priv->reg->dest_addr);

	size = param->size;
	switch (param->data_width) {
	case APB_DMAB_DATA_WIDTH_1:
		break;
	case APB_DMAB_DATA_WIDTH_2:
		size >>= 1;
		break;
	case APB_DMAB_DATA_WIDTH_4:
	default:
		size >>= 2;
		break;
	}

	if (param->burst_mode)
		size >>= 2;

	writel(size, &priv->reg->cycles);
	cmd.ul = readl(&priv->reg->command.ul);
	cmd.bits.data_width = param->data_width;
	if (param->dest_sel) { /* AHB */
		/*dmac_inv_range((void *)param->dest_addr,
			(void *)param->dest_addr + param->size);*/
		cmd.bits.dest_req_no = 0;
	} else { /* APB */
		cmd.bits.dest_req_no = priv->req_no;
	}
	cmd.bits.dest_sel = param->dest_sel;
	if (param->source_sel) { /* AHB */
		dmac_flush_range((void *)param->source_addr,
			(void *)param->source_addr + param->size);
		cmd.bits.source_req_no = 0;
	} else { /* APB */
		cmd.bits.source_req_no = priv->req_no;
	}
	cmd.bits.source_sel = param->source_sel;
	cmd.bits.burst = param->burst_mode;
	cmd.bits.dest_inc = param->dest_inc;
	cmd.bits.source_inc = param->source_inc;
	writel(cmd.ul, &priv->reg->command.ul);

	cmd.ul = readl(&priv->reg->command.ul);
	dbg_printk(KERN_INFO
		"apb_dma_conf: cmd.ul=%x read from &priv->reg->command.ul=%x\n"
		, cmd.ul, (unsigned int)&priv->reg->command.ul);

	spin_unlock_irqrestore(&apb_dma_lock, flags);
}

void apb_dma_enable(struct apb_dma_priv *priv)
{
	unsigned long flags;
	unsigned int cmd;

	spin_lock_irqsave(&apb_dma_lock, flags);
	cmd = readl(&priv->reg->command.ul);
	/* dbg_printk(KERN_INFO "apb_dma_enable: cmd=%x read from
		"&priv->reg->command.ul=%x\n", cmd,
		(unsigned int)&priv->reg->command.ul);*/
	cmd |= APB_DMA_ENABLE;
	writel(cmd, &priv->reg->command.ul);
	cmd = readl(&priv->reg->command.ul);
	/* dbg_printk(KERN_INFO "apb_dma_enable: readback:"
		"(cmd | APB_DMA_ENABLE)=%x\n", cmd);*/
	spin_unlock_irqrestore(&apb_dma_lock, flags);
}

void apb_dma_disable(struct apb_dma_priv *priv)
{
	unsigned long flags;
	unsigned int cmd;

	spin_lock_irqsave(&apb_dma_lock, flags);
	cmd = readl(&priv->reg->command.ul);
	cmd &= ~APB_DMA_ENABLE;
	writel(cmd, &priv->reg->command.ul);
	spin_unlock_irqrestore(&apb_dma_lock, flags);
}

static irqreturn_t moxart_dma_interrupt(int irq, void *devid)
{
	int i;
	unsigned int cmd;
	struct apb_dma_priv *priv = apb_dma_channel;

	/* dbg_printk(KERN_INFO "moxart_dma_interrupt\n"); */
	for (i = 0; i < APB_DMA_MAX_CHANNEL; i++, priv++) {
		cmd = readl(&priv->reg->command.ul);
		if (cmd & APB_DMA_FIN_INT_STS)
			cmd &= ~APB_DMA_FIN_INT_STS;
		if (cmd & APB_DMA_ERR_INT_STS) {
			cmd &= ~APB_DMA_ERR_INT_STS;
			priv->error_flag = 1;
		}
		if (priv->used_flag && priv->irq_handler)
			priv->irq_handler(priv->irq_handler_param);
		priv->error_flag = 0;
		writel(cmd, &priv->reg->command.ul);
	}

	return IRQ_HANDLED;
}

static struct irqaction moxart_dma_irq = {
	.name		= "MOXART APB DMA",
	.flags		= IRQF_DISABLED,
	.handler	= moxart_dma_interrupt,
};

static int moxart_dma_probe(struct platform_device *pdev)
{
	int i;
	struct apb_dma_priv *priv = apb_dma_channel;

	memset(apb_dma_channel, 0, sizeof(apb_dma_channel));
	spin_lock_init(&apb_dma_lock);

	for (i = 0; i < APB_DMA_MAX_CHANNEL; i++)
		priv[i].reg = (struct apb_dma_reg *)
			(IO_ADDRESS(MOXART_APB_DMA_BASE)
			+ 0x80 + i*sizeof(struct apb_dma_reg));

	moxart_int_set_irq(IRQ_APB_DMA, EDGE, H_ACTIVE);
	setup_irq(IRQ_APB_DMA, &moxart_dma_irq);

	dev_info(&pdev->dev, "MOXART APB DMA: finished moxart_dma_probe\n");

	return 0;
}

static int moxart_dma_remove(struct platform_device *pdev)
{
	return 0;
}

static struct platform_driver moxart_dma_driver = {
	.probe		= moxart_dma_probe,
	.remove		= __devexit_p(moxart_dma_remove),
	.driver		= {
		.name	= "MOXART_APB_DMA",
		.owner	= THIS_MODULE,
	},
};

static __init int moxart_init(void)
{
	return platform_driver_register(&moxart_dma_driver);
}

static __exit void moxart_exit(void)
{
	platform_driver_unregister(&moxart_dma_driver);
}

module_init(moxart_init);
module_exit(moxart_exit);

MODULE_AUTHOR("Jonas Jensen <jonas.jensen@gmail.com>");
MODULE_DESCRIPTION("MOXART APB DMA driver");
MODULE_LICENSE("GPL");
MODULE_VERSION(DRV_VERSION);
