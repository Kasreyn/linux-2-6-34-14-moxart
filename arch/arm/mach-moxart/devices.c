/* Copyright (C) 2013 Jonas Jensen <jonas.jensen@gmail.com>
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version. */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/io.h>
#include <linux/platform_device.h>
#include <linux/serial_8250.h>
#include <linux/mtd/physmap.h>

#include <mach/irqs.h>
#include <mach/hardware.h>
#include <mach/board.h>

static struct plat_serial8250_port __initdata serial_platform_data[] = {
	{
		.membase	= (void *)IO_ADDRESS(MOXART_UART1_BASE),
		.mapbase	= MOXART_UART1_BASE,
/*		.iobase		= IO_ADDRESS(MOXART_UART1_BASE), */
		.irq		= IRQ_UART,
		.uartclk	= UART_CLK,
		.regshift	= 2,
		.iotype		= UPIO_MEM,
		.type		= PORT_16550A,
		.flags		= UPF_SHARE_IRQ | UPF_SKIP_TEST,
	},
	/*{
		.membase	= (void *)IO_ADDRESS(MOXART_UART2_BASE),
		.mapbase	= MOXART_UART2_BASE,
		.iobase		= IO_ADDRESS(MOXART_UART2_BASE),
		.irq		= IRQ_UART,
		.uartclk	= UART_CLK,
		.regshift	= 2,
		.iotype		= UPIO_MEM,
		.type		= PORT_16550A,
		.flags		= UPF_SHARE_IRQ | UPF_BOOT_AUTOCONF |
					  UPF_SKIP_TEST | UPF_FIXED_TYPE,
	},*/
	{},
};

static struct platform_device __initdata serial_device = {
	.name	= "serial8250",
	.id	= PLAT8250_DEV_PLATFORM,
	.dev	= {
		.platform_data = &serial_platform_data,
	},
};

void __init moxart_register_uart(void)
{
	platform_device_register(&serial_device);
}

static struct uart_port moxart_serial_ports[] = {
	{
		.iobase		= IO_ADDRESS(MOXART_UART1_BASE),
		.mapbase	= MOXART_UART1_BASE,
		.irq		= IRQ_UART,
		.flags		= UPF_SKIP_TEST | UPF_SHARE_IRQ,
		.iotype		= UPIO_PORT,
		.regshift	= 2,
		.uartclk	= UART_CLK,
		.line		= 0,
		.type		= PORT_16550A,
		.fifosize	= 16
	},
	{
		.iobase		= IO_ADDRESS(MOXART_UART2_BASE),
		.mapbase	= MOXART_UART2_BASE,
		.irq		= IRQ_UART,
		.flags		= UPF_SKIP_TEST | UPF_SHARE_IRQ,
		.iotype		= UPIO_PORT,
		.regshift	= 2,
		.uartclk	= UART_CLK,
		.line		= 1,
		.type		= PORT_16550A,
		.fifosize	= 16
	}
};

void __init moxart_serial_init(void)
{
    early_serial_setup(&moxart_serial_ports[0]);
    early_serial_setup(&moxart_serial_ports[1]);
}

static struct resource __initdata moxart_mac1_resources[] = {
	[0] = {
		.start	= IO_ADDRESS(MOXART_FTMAC1_BASE),
		.end	= IO_ADDRESS(MOXART_FTMAC1_BASE) + SZ_4K - 1,
		.flags	= IORESOURCE_MEM,
	},
	[1] = {
		.start	= IRQ_MAC1,
		.end	= IRQ_MAC1,
		.flags	= IORESOURCE_IRQ,
	},
};

static struct resource __initdata moxart_mac2_resources[] = {
	[0] = {
		.start	= IO_ADDRESS(MOXART_FTMAC2_BASE),
		.end	= IO_ADDRESS(MOXART_FTMAC2_BASE) + SZ_4K - 1,
		.flags	= IORESOURCE_MEM,
	},
	[1] = {
		.start	= IRQ_MAC2,
		.end	= IRQ_MAC2,
		.flags	= IORESOURCE_IRQ,
	},
};

static struct moxart_eth_data __initdata moxart_mac1_data = {
	.base_addr = IO_ADDRESS(MOXART_FTMAC1_BASE),
	.irq = IRQ_MAC1,
	.mac_addr_flash_offset = MOXART_FLASH_ETH1_OFFSET,
};

static struct moxart_eth_data __initdata moxart_mac2_data = {
	.base_addr = IO_ADDRESS(MOXART_FTMAC2_BASE),
	.irq = IRQ_MAC2,
	.mac_addr_flash_offset = MOXART_FLASH_ETH2_OFFSET,
};

static struct platform_device __initdata moxart_mac1_device = {
	.name		= "MOXART_RTL8201CP",
	.id			= 0,
	.dev		= {
		.platform_data = &moxart_mac1_data,
	},
	.resource	= moxart_mac1_resources,
	.num_resources	= ARRAY_SIZE(moxart_mac1_resources),
};

static struct platform_device __initdata moxart_mac2_device = {
	.name		= "MOXART_RTL8201CP",
	.id			= 1,
	.dev		= {
		.platform_data = &moxart_mac2_data,
	},
	.resource	= moxart_mac2_resources,
	.num_resources	= ARRAY_SIZE(moxart_mac2_resources),
};

/*void __init moxart_add_ethernet_devices(struct moxart_eth_data *data)*/
void __init moxart_add_ethernet_devices(void)
{
	platform_device_register(&moxart_mac1_device);
	platform_device_register(&moxart_mac2_device);
}

static struct resource __initdata moxart_rtc_resource[] = {
	[0] = {
		.start	= IO_ADDRESS(MOXART_PMU_BASE),
		.end	= IO_ADDRESS(MOXART_PMU_BASE) + SZ_4K - 1,
		.flags	= IORESOURCE_MEM,
	},
};

static struct platform_device __initdata moxart_rtc_device = {
	.name		= "MOXART_RTC",
	.id			= 1,
	.resource	= moxart_rtc_resource,
	.num_resources	= ARRAY_SIZE(moxart_rtc_resource),
};

void __init moxart_add_rtc_device()
{
	platform_device_register(&moxart_rtc_device);
}

static struct resource __initdata moxart_dma_resource[] = {
	[0] = {
		.start	= IO_ADDRESS(MOXART_APB_DMA_BASE),
		.end	= IO_ADDRESS(MOXART_APB_DMA_BASE) + SZ_4K - 1,
		.flags	= IORESOURCE_MEM,
	},
};

static struct platform_device __initdata moxart_dma_device = {
	.name		= "MOXART_APB_DMA",
	.id			= 1,
	.resource	= moxart_dma_resource,
	.num_resources	= ARRAY_SIZE(moxart_dma_resource),
};

void __init moxart_add_dma_device()
{
	platform_device_register(&moxart_dma_device);
}
